package Game;

import java.util.ArrayList;

public class Player implements Runnable {

    private int playerID;
    ArrayList playerHand;
    private static boolean winState = false;
    protected Deck drawDeck;
    protected Deck discardDeck;
    private static int winner;
    Thread t;

    public Player(int playerID) {
        this.playerID = playerID;
        playerHand = new ArrayList();
    }

    public void run() {
        checkWin();
        sleep();
        while (!winState) {
            drawDiscard();
            checkWin();
            sleep();
        }
    }

    public void show() {
        System.out.println("Player " + playerID);
        for (int i = 0; i < playerHand.size(); i++) {
            System.out.println(playerHand.get(i));
        }
    }

    public int getID() {
        return playerID;
    }

    public synchronized void checkWin() {
        //check all cards if they are the same
        for (int i = 0; i < 3; i++) {
            if (playerHand.get(i) != playerHand.get(i + 1)) {
                return;
            }
        }
        //if reach here means all cards are the same
        if(winState == true)
            return;
        winState = true;
        winner = playerID;
    }

    public synchronized void addCard(int card) {
        playerHand.add(card);
    }

    public synchronized void drawDiscard() {
        addCard(drawDeck.pickCard());
        for (int i = 0; i < playerHand.size(); i++) {
            //game strategy
            if ((Integer) playerHand.get(i) != playerID) {
                int card = (Integer) playerHand.remove(i);
                discardDeck.addCard(card);
                break;
            }
        }
    }

    public void assignDecks(Deck draw, Deck discard) {
        this.drawDeck = draw;
        this.discardDeck = discard;
    }

    public void sleep() {
        try {
            t.sleep(20);
        } catch (Exception e) {
        }
    }

    public void join() {
        try {
            t.join();
        } catch (Exception e) {
        }
    }

    public void start() {
        t = new Thread(this);
        t.start();
    }

    public int getWinner() {
        return winner;
    }

}
