package Game;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;


public class CardGame {

    protected Player[] players;
    protected Deck[] decks;
    protected ArrayList intialDeck;
    protected String fileName;
    protected static int numOfPlayers;

    public static CardGame cardGame;
    public static boolean end = false;

    public CardGame(String fileName, int numOfPlayers) {

        this.fileName = fileName;
        this.numOfPlayers = numOfPlayers;
        intialDeck = new ArrayList();
    }

    protected void readFile() throws FileNotFoundException {
        File f = new File(fileName);
        if(!f.exists()) 
        {
           System.out.println("Failed to find file");
           System.out.println("Try moving deck.txt file to desktop");
           System.exit(0);
        }
        Scanner s = new Scanner(f);
        while (s.hasNextInt()) {
            intialDeck.add(s.nextInt());
        }
    }

    protected void checkEnoughCards() {
        if (intialDeck.size() < (numOfPlayers * 8)) {
            System.out.println("Not enough cards in deck file");
            System.exit(0);
        }
    }

    protected void createPlayerDecks() {
        players = new Player[numOfPlayers];
        decks = new Deck[numOfPlayers];
        for (int i = 0; i < numOfPlayers; i++) {
            players[i] = new Player(i + 1);
            decks[i] = new Deck(i + 1);
        }
    }

    protected void distributePlayerCards() {
        int j = 0;
        for (int i = 0; i < 4 * numOfPlayers; i++) {
            if (j < numOfPlayers) {
                players[j].addCard((Integer) intialDeck.remove(0));
                j++;
            } else {
                j = 0;
                players[j].addCard((Integer) intialDeck.remove(0));
                j++;
            }
        }
    }

    protected void distributeDeckCards() {
        int j = 0;
        for (int i = 4 * numOfPlayers; i < intialDeck.size(); i++) {
            if (j < numOfPlayers) {
                decks[j].addCard((Integer) intialDeck.remove(0));
                j++;
            } else {
                j = 0;
                decks[j].addCard((Integer) intialDeck.remove(0));
                j++;
            }
        }
    }

    protected void assignDeck() {
        players[0].assignDecks(decks[players.length - 1], decks[0]);
        for (int i = 1; i < players.length; i++) {
            players[i].assignDecks(decks[i - 1], decks[i]);
        }
    }

    protected void startGame() {
        for (Player player : players) {
            player.start();
        }
    }
    
    protected void displayWinner()
    {
        System.out.println("Player " + players[0].getWinner() + " has won the game!");

    }

    protected void joinPlayers() {
        for (Player player : players) {
            player.join();
        }
    }


    public static void main(String[] args) throws FileNotFoundException {

        Scanner s = new Scanner(System.in);
        int num;
        String fileName;

        System.out.println("HOW MANY PLAYERS: ");
        num = s.nextInt();

        if (num < 2 || num > 8) {
            //PLAYER RANGE FROM 2 TO 8
            System.out.println("Number of Players out of range");
            return;
        }

        System.out.println("DECK NAME/LOCATION: ");
        fileName = s.next();

        CardGame cardGame = new CardGame(fileName, num);

        //put each integer from deck.txt into card
        cardGame.readFile();

        //check number of players
        cardGame.checkEnoughCards();

        //constructs players and decks
        cardGame.createPlayerDecks();

        //distribute cards to players
        cardGame.distributePlayerCards();

        //distribute the remain cards to the deck
        cardGame.distributeDeckCards();

        //assign Decks to Players
        cardGame.assignDeck();

        //start the game
        cardGame.startGame();

        //joins the players thread
        cardGame.joinPlayers();
        
        //displays winner
        cardGame.displayWinner();
        
    }
}
