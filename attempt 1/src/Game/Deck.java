package Game;

import java.util.ArrayList;

public class Deck {
    ArrayList cards;
    int deckNo;
    
    public Deck(int deckNo)
    {
        cards = new ArrayList();
        this.deckNo = deckNo;
    }
    
    public void addCard(int card)
    {
        cards.add(card);
    }
    
    public int pickCard()
    {
        while(true)
        {
            try
            {
                return (Integer) cards.remove(0);
            }
            catch(Exception e)
            {
                System.out.println("No cards in deck");
                
            }
        }
    }
    
}
