package Game;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author joechan
 */
public class DeckTest {
    Deck testdeck;
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("Testing Deck class.");
    }
    
    @AfterClass
    public static void tearDownClass() {
        System.out.println("Finish testing Deck class.");
    }
    
    @Before
    public void setUp() {
        testdeck = new Deck(1);
    }
    
    @After
    public void tearDown() {
        testdeck = null;
    }

    
    @Test
    public void testAddCard() {
        System.out.println("Testing addCard method.");
        int card = 1;
        testdeck.addCard(card);
        ArrayList expResult = new ArrayList(); 
        expResult.add(1);
        //check if card is added to testdeck
        assertEquals(1, testdeck.cards.size());
        assertEquals(expResult, testdeck.cards);
    }

    @Test
    public void testPickCard() {
        System.out.println("Testing pickCard method.");
        int expResult = 1;
        testdeck.addCard(1);
        int result = testdeck.pickCard();
        //check returns 1
        assertEquals(expResult, result);
        //check it removes the element
        assertEquals(0,testdeck.cards.size());
    }
    
}
