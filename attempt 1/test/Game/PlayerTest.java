package Game;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


public class PlayerTest {
    Player testplayer;
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("Testing Player class.");
    }
    
    @AfterClass
    public static void tearDownClass() {
        System.out.println("Finsihed testing Player class.");
    }
    
    @Before
    public void setUp() {
        testplayer = new Player(2);
        testplayer.playerHand.add(2);
        testplayer.playerHand.add(2);
        testplayer.playerHand.add(2);
        testplayer.playerHand.add(2);
        
    }
    
    @After
    public void tearDown() {
        testplayer = null;
    }

    
//    @Test
//    public void testRun() {
//        testplayer.run();
//    }

    
//    @Test
//    public void testCheckWin() {
//        testplayer.checkWin();
//    }

    @Test
    public void testAddCard() {
        System.out.println("Testing addCard method.");
        Player expObject = new Player(2);
        expObject.playerHand.add(2);
        expObject.playerHand.add(2);
        expObject.playerHand.add(2);
        expObject.playerHand.add(2);
        expObject.playerHand.add(1);
        testplayer.addCard(1);
        //check returns both players are the same
        for (int i = 0; i<testplayer.playerHand.size();i++)
            assertEquals(expObject.playerHand.get(i), testplayer.playerHand.get(i));
        //check save of hands also the same
        assertEquals(expObject.playerHand.size(),testplayer.playerHand.size());
    }
    
    @Test
    public void testAssignDecks() {
        System.out.println("Testing assignDecks method.");
        Deck left = new Deck(2);
        Deck right = new Deck(3);
        testplayer.assignDecks(left, right);
        assertEquals(testplayer.drawDeck.deckNo,2);
        assertEquals(testplayer.discardDeck.deckNo,3);
    }

    @Test
    public void testDrawDiscard() {
        System.out.println("Testing drawDiscard method.");
        //create enviroment
        Deck left = new Deck(2);
        Deck right = new Deck(3);
        testplayer.assignDecks(left, right);
        //left deck has one card [1]
        left.cards.add(1);
        testplayer.drawDiscard();
        //new card added to right deck
        assertEquals(right.cards.size(), 1);
        //card removed from left deck
        assertEquals(left.cards.size(), 0);
        
    }

//    @Test
//    public void testStart() {
//        testplayer.start();
//    }
//
//    
//    @Test
//    public void testJoin() {
//        testplayer.join();
//    }
//
//    
//    @Test
//    public void testSleep() {
//        testplayer.sleep();
//    }
    
}
