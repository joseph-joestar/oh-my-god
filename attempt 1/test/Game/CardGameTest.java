package Game;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class CardGameTest {

    CardGame smallinstance;
    CardGame biginstance;

    public CardGameTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        System.out.println("Testing CardGame class.");
    }

    @AfterClass
    public static void tearDownClass() {
        System.out.println("Finished testing CardGame class.");
    }

    @Before
    public void setUp() {
        //smalltestdeck.txt contains 1 2 1 2
        smallinstance = new CardGame("smalltestdeck.txt", 2);
        //smalltestdeck.txt contains 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2
        biginstance = new CardGame("bigtestdeck.txt", 2);
    }

    @After
    public void tearDown() {
        smallinstance = null;
        biginstance = null;
    }

    @Test
    public void testConstructor() {
        System.out.println("Testing Constructor.");
        assertEquals(smallinstance.fileName, "smalltestdeck.txt");
        assertEquals(smallinstance.numOfPlayers, 2);
    }

    @Test
    public void testReadFile() throws Exception {
        System.out.println("Testing readFile method.");
        ArrayList expResult = new ArrayList();
        expResult.add(1);
        expResult.add(2);
        expResult.add(1);
        expResult.add(2);
        for (int i = 0; i < smallinstance.intialDeck.size(); i++) {
            assertEquals(smallinstance.intialDeck.get(i), expResult.get(i));
        }
    }

//    @Test
//    public void testCheckEnoughCards() {
//        TESTED WITHIN THE METHOD 
//    }
    @Test
    public void testCreatePlayerDecks() {
        System.out.println("Testing createPlayerDecks method.");
        biginstance.createPlayerDecks();
        assertEquals(biginstance.players.length, 2);
        assertEquals(biginstance.decks.length, 2);
    }

    @Test
    public void testDistributePlayerCards() throws Exception{
        System.out.println("Testing distributePlayerCards method.");
        biginstance.readFile();
        biginstance.createPlayerDecks();
        biginstance.distributePlayerCards();
        ArrayList expResult = new ArrayList();
        expResult.add(1);
        expResult.add(1);
        expResult.add(1);
        expResult.add(1);
        for (int i = 0; i < biginstance.players[0].playerHand.size(); i++) {
            assertEquals(biginstance.players[0].playerHand.get(i), expResult.get(i));
        }
    }

    @Test
    public void testDistributeDeckCards() throws Exception{
        System.out.println("Testing distributeDeckCards method.");
        biginstance.readFile();
        biginstance.createPlayerDecks();
        biginstance.distributePlayerCards();
        biginstance.distributeDeckCards();
        ArrayList expResult = new ArrayList();
        expResult.add(1);
        expResult.add(1);
        expResult.add(1);
        expResult.add(1);
        for (int i = 0; i<biginstance.decks[0].cards.size();i++)
            assertEquals(biginstance.decks[0].cards.get(i),expResult.get(i));
    }

    @Test
    public void testAssignDeck() {
        biginstance.createPlayerDecks();
        biginstance.assignDeck();
        assertEquals(biginstance.players[0].drawDeck,biginstance.decks[1]);
        assertEquals(biginstance.players[0].discardDeck,biginstance.decks[0]);
    }

//    @Test
//    public void testStartGame() {
//
//    }

//    @Test
//    public void testDisplayWinner() {
//    }

//    @Test
//    public void testJoinPlayers() {
//    }


}
